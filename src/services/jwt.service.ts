interface IJsonwebtoken {
  sign: (payload: object, secret: string, options: { expiresIn?: string }) => string;
  verify: (token: string, key: string) => string | object;
  decode: (token: string) => null | { [key: string]: any } | string;
}

interface IRawTokenData<T> { payload: T; iat: number; exp: number; }
export interface ITokenData<T> { payload: T; issuedAt: number; expiresAt: number; }
export interface IJWTService {
  sign<T extends object>(payload: T, key?: string, expiresIn?: string): string;
  decode<T>(token: string, key?: string | null): ITokenData<T>;
  verify(token: string, key: string): boolean;
}

export class JWTService implements IJWTService {
  public constructor(
    private readonly defaultKey: string = Math.random().toString(),
    private readonly jsonwebtokenModule: IJsonwebtoken,
    private readonly defaultExpiresIn = "7 days",
  ) { }

  public sign<T extends object>(payload: T, key: string = this.defaultKey, expiresIn: string = this.defaultExpiresIn): string {
    const token = this.jsonwebtokenModule.sign({ payload }, key, { expiresIn });
    if (typeof token !== "string") { throw new Error("Unable to sign"); }
    return token;
  }
  public decode<T>(token: string, key: string | null = this.defaultKey): ITokenData<T> {
    try {
      const decodedToken: string | IRawTokenData<T> = key === null ? this.jsonwebtokenModule.decode(token) as string | IRawTokenData<T> : this.jsonwebtokenModule.verify(token, key) as string | IRawTokenData<T>;
      if (typeof decodedToken !== "object") {
        throw new Error("Token did'n resolve to an object");
      } else {
        return {
          payload: decodedToken.payload,
          issuedAt: decodedToken.iat,
          expiresAt: decodedToken.exp,
        };
      }
    } catch (error) {
      throw error;
    }
  }
  public verify(token: string, key: string = this.defaultKey): boolean {
    try {
      this.decode(token, key);
      return true;
    } catch { return false; }
  }
}
