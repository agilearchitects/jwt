// Lib
import { expect } from "chai";
import { describe, it } from "mocha";
import jsonwebtoken from "jsonwebtoken";

// JWT service
import { JWTService } from "./jwt.service";


describe("JWTService", () => {
  it("Should be able to sign data", () => {
    const jwtService = new JWTService("ABC", jsonwebtoken);
    const token = jwtService.sign({ foo: "bar" });
    expect(token).not.empty
  });
  it("Should be able to decode token", () => {
    const key = "ABC";
    const jwtService = new JWTService(key, jsonwebtoken);
    const data = { foo: "bar" };
    const token = jwtService.sign(data);
    const decoded = jwtService.decode(token, key);
    expect(decoded.payload).to.deep.equal(data);
  });
  it("Should be able to decode token without key", () => {
    const key = "ABC";
    const jwtService = new JWTService(key, jsonwebtoken);
    const data = { foo: "bar" };
    const token = jwtService.sign(data);
    const decoded = jwtService.decode(token, null);
    expect(decoded.payload).to.deep.equal(data);
  });
  it("Should fail on decode with wrong key", () => {
    const key = "ABC";
    const jwtService = new JWTService(key, jsonwebtoken);
    const data = { foo: "bar" };
    const token = jwtService.sign(data);
    expect(jwtService.decode.bind(token, "key")).to.throw;
  });
  it("Should fail on decode with no key procided", () => {
    const key = "ABC";
    const jwtService = new JWTService(key, jsonwebtoken);
    const data = { foo: "bar" };
    const token = jwtService.sign(data);
    expect(jwtService.decode.bind(token)).to.throw;
  });
  it("Should be able to verify", () => {
    const key = "ABC";
    const jwtService = new JWTService(key, jsonwebtoken);
    const token = jwtService.sign({ foo: "bar" });
    const verified = jwtService.verify(token)
    expect(verified).to.be.true
  });
});
